package com.twuc.webApp;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.webApp.model.Question;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest
@AutoConfigureMockMvc
class TableControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    void plusTable() throws Exception {

        String result =
                "1+1=2\n" +
                "2+1=3  2+2=4\n" +
                "3+1=4  3+2=5  3+3=6\n" +
                "4+1=5  4+2=6  4+3=7  4+4=8\n" +
                "5+1=6  5+2=7  5+3=8  5+4=9  5+5=10\n" +
                "6+1=7  6+2=8  6+3=9  6+4=10 6+5=11 6+6=12\n" +
                "7+1=8  7+2=9  7+3=10 7+4=11 7+5=12 7+6=13 7+7=14\n" +
                "8+1=9  8+2=10 8+3=11 8+4=12 8+5=13 8+6=14 8+7=15 8+8=16\n" +
                "9+1=10 9+2=11 9+3=12 9+4=13 9+5=14 9+6=15 9+7=16 9+8=17 9+9=18\n";

        mockMvc.perform(get("/api/tables/plus"))
                .andExpect(status().isOk());
    }

    @Test
    void multiplyTable() throws Exception {

        String result =
                "1*1=1\n" +
                "2*1=2 2*2=4\n" +
                "3*1=3 3*2=6  3*3=9\n" +
                "4*1=4 4*2=8  4*3=12 4*4=16\n" +
                "5*1=5 5*2=10 5*3=15 5*4=20 5*5=25\n" +
                "6*1=6 6*2=12 6*3=18 6*4=24 6*5=30 6*6=36\n" +
                "7*1=7 7*2=14 7*3=21 7*4=28 7*5=35 7*6=42 7*7=49\n" +
                "8*1=8 8*2=16 8*3=24 8*4=32 8*5=40 8*6=48 8*7=56 8*8=64\n" +
                "9*1=9 9*2=18 9*3=27 9*4=36 9*5=45 9*6=54 9*7=63 9*8=72 9*9=81\n";

        String result2 =
                "<ol>\n" +
                "  <li>\n" +
                "    <ol>\n" +
                "      <li>1*1=1</li>\n" +
                "    </ol>\n" +
                "  <li>\n" +
                "  <li>\n" +
                "    <ol>\n" +
                "      <li>2*1=2</li>\n" +
                "      <li>2*2=4</li>\n" +
                "    </ol>\n" +
                "  <li>\n" +
                "  <li>\n" +
                "    <ol>\n" +
                "      <li>3*1=3</li>\n" +
                "      <li>3*2=6</li>\n" +
                "      <li>3*3=9</li>\n" +
                "    </ol>\n" +
                "  <li>\n" +
                "  <li>\n" +
                "    <ol>\n" +
                "      <li>4*1=4</li>\n" +
                "      <li>4*2=8</li>\n" +
                "      <li>4*3=12</li>\n" +
                "      <li>4*4=16</li>\n" +
                "    </ol>\n" +
                "  <li>\n" +
                "  <li>\n" +
                "    <ol>\n" +
                "      <li>5*1=5</li>\n" +
                "      <li>5*2=10</li>\n" +
                "      <li>5*3=15</li>\n" +
                "      <li>5*4=20</li>\n" +
                "      <li>5*5=25</li>\n" +
                "    </ol>\n" +
                "  <li>\n" +
                "  <li>\n" +
                "    <ol>\n" +
                "      <li>6*1=6</li>\n" +
                "      <li>6*2=12</li>\n" +
                "      <li>6*3=18</li>\n" +
                "      <li>6*4=24</li>\n" +
                "      <li>6*5=30</li>\n" +
                "      <li>6*6=36</li>\n" +
                "    </ol>\n" +
                "  <li>\n" +
                "  <li>\n" +
                "    <ol>\n" +
                "      <li>7*1=7</li>\n" +
                "      <li>7*2=14</li>\n" +
                "      <li>7*3=21</li>\n" +
                "      <li>7*4=28</li>\n" +
                "      <li>7*5=35</li>\n" +
                "      <li>7*6=42</li>\n" +
                "      <li>7*7=49</li>\n" +
                "    </ol>\n" +
                "  <li>\n" +
                "  <li>\n" +
                "    <ol>\n" +
                "      <li>8*1=8</li>\n" +
                "      <li>8*2=16</li>\n" +
                "      <li>8*3=24</li>\n" +
                "      <li>8*4=32</li>\n" +
                "      <li>8*5=40</li>\n" +
                "      <li>8*6=48</li>\n" +
                "      <li>8*7=56</li>\n" +
                "      <li>8*8=64</li>\n" +
                "    </ol>\n" +
                "  <li>\n" +
                "  <li>\n" +
                "    <ol>\n" +
                "      <li>9*1=9</li>\n" +
                "      <li>9*2=18</li>\n" +
                "      <li>9*3=27</li>\n" +
                "      <li>9*4=36</li>\n" +
                "      <li>9*5=45</li>\n" +
                "      <li>9*6=54</li>\n" +
                "      <li>9*7=63</li>\n" +
                "      <li>9*8=72</li>\n" +
                "      <li>9*9=81</li>\n" +
                "    </ol>\n" +
                "  <li>\n" +
                "</ol>";

        mockMvc.perform(get("/api/tables/multiply"))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.content().string(result2));
    }

    @Test
    void should_return_400_when_param_is_not_valid() throws Exception {
        mockMvc.perform(get("/api/tables/plus?start=-1&end=3"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void should_return_plus_table_and_range_is_3_to_5() throws Exception {

        String result =
                "3+3=6\n" +
                "4+3=7 4+4=8\n" +
                "5+3=8 5+4=9 5+5=10\n";

        mockMvc.perform(get("/api/tables/plus?start=3&end=5"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0][0]").value("3+3=6"))
                .andExpect(jsonPath("$[1][0]").value("4+3=7"))
                .andExpect(jsonPath("$[1][1]").value("4+4=8"))
                .andExpect(jsonPath("$[2][0]").value("5+3=8"))
                .andExpect(jsonPath("$[2][1]").value("5+4=9"))
                .andExpect(jsonPath("$[2][2]").value("5+5=10"));
    }

    @Test
    void should_return_200_when_check_question() throws Exception {
        String questionJson = new ObjectMapper()
                .writeValueAsString(new Question(1, 2, "+", 3));
        mockMvc.perform(post("/api/check")
                .contentType(APPLICATION_JSON)
                .content(questionJson))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.correct").value(true));
    }

    @Test
    void should_return_400_given_expected_result_is_null() throws Exception {
        String questionJson = new ObjectMapper()
                .writeValueAsString(new Question(1, 2, "+"));
        mockMvc.perform(post("/api/check")
                .contentType(APPLICATION_JSON)
                .content(questionJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    void should_return_200_given_check_type_is_less() throws Exception {
        String questionJson = new ObjectMapper()
                .writeValueAsString(new Question(1, 2, "+", 4, "<"));
        mockMvc.perform(post("/api/check")
                .contentType(APPLICATION_JSON)
                .content(questionJson))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.correct").value(true));
    }

    @Test
    void should_return_200_when_check_type_is_default() throws Exception {
        String questionJson = new ObjectMapper()
                .writeValueAsString(new Question(1, 2, "+", 3));
        mockMvc.perform(post("/api/check")
                .contentType(APPLICATION_JSON)
                .content(questionJson))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.correct").value(true));
    }
}
