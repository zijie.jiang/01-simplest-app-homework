package com.twuc.webApp.model;


import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


public class Question {

    @NotNull
    private Integer operandLeft;

    @NotNull
    private Integer operandRight;

    @NotBlank
    private String operation;

    @NotNull
    private Integer expectedResult;

    private String checkType = "";

    public Question() {

    }

    public Question(Integer operandLeft, Integer operandRight, String operation) {
        this.operandLeft = operandLeft;
        this.operandRight = operandRight;
        this.operation = operation;
    }

    public Question(Integer operandLeft, Integer operandRight, String operation, Integer expectedResult) {
        this.operandLeft = operandLeft;
        this.operandRight = operandRight;
        this.operation = operation;
        this.expectedResult = expectedResult;
    }

    public Question(Integer operandLeft, Integer operandRight, String operation, Integer expectedResult, String checkType) {
        this.operandLeft = operandLeft;
        this.operandRight = operandRight;
        this.operation = operation;
        this.expectedResult = expectedResult;
        this.checkType = checkType;
    }

    public Integer getOperandLeft() {
        return operandLeft;
    }

    public Integer getOperandRight() {
        return operandRight;
    }

    public String getOperation() {
        return operation;
    }

    public Integer getExpectedResult() {
        return expectedResult;
    }

    public String getCheckType() {
        return checkType;
    }
}
