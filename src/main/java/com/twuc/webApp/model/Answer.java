package com.twuc.webApp.model;

public class Answer {
    private boolean correct;

    public Answer(boolean correct) {
        this.correct = correct;
    }

    public Answer() {

    }

    public boolean isCorrect() {
        return correct;
    }
}
