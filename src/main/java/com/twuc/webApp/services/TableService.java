package com.twuc.webApp.services;

import com.twuc.webApp.model.Answer;
import com.twuc.webApp.model.Question;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


//TODO 列遍历
@Service
public class TableService {
    public List<List<String>> plusTable(Integer start, Integer end) {
        StringBuilder builder = new StringBuilder();
        for (int i = start; i <= end; i++) {
            for (int j = start; j <= i; j++) {
                int maxLenOnCol = String.valueOf(j + end).length();
                builder.append(i).append('+').append(j).append('=').append(i + j);
                if (j != i) {
                    for (int k = String.valueOf(i + j).length(); k <= maxLenOnCol; k++) {
                        builder.append(' ');
                    }
                }
            }
            builder.append("\n");
        }
        return convertToList(builder.toString());
    }

    public String multiplyTable(Integer start, Integer end) {
        StringBuilder builder = new StringBuilder();
        for (int i = start; i <= end; i++) {
            for (int j = start; j <= i; j++) {
                int maxLenOnCol = String.valueOf(j * end).length();
                builder.append(i).append('*').append(j).append('=').append(i * j);
                if (j != i) {
                    for (int k = String.valueOf(i * j).length(); k <= maxLenOnCol; k++) {
                        builder.append(' ');
                    }
                }
            }
            builder.append("\n");
        }
        return convertToHTML(builder.toString());
    }

    public Answer check(Question question) {
        int result = 0;
        switch (question.getOperation()) {
            case "+":
                result = question.getOperandLeft() + question.getOperandRight();
                break;
            case "*":
                result = question.getOperandLeft() * question.getOperandRight();
                break;
            default:
                break;
        }

        if (question.getCheckType().equals("<")) {
            return new Answer(result < question.getExpectedResult());
        } else if (question.getCheckType().equals(">")) {
            return new Answer(result > question.getExpectedResult());
        } else {
            return new Answer(result == question.getExpectedResult());
        }
    }

    private List<List<String>> convertToList(String source) {
        List<List<String>> result = new ArrayList<>();
        String[] lines = source.split("\n");
        for (String line : lines) {
            String[] statement = line.split("\\s+");
            result.add(Arrays.asList(statement));
        }
        return result;
    }

    private String convertToHTML(String source) {
        List<List<String>> array = convertToList(source);
        StringBuilder builder = new StringBuilder("<ol>\n");
        for (List<String> multiple : array) {
            builder.append("  <li>\n").append("    <ol>\n");
            for (String line : multiple) {
                builder.append("      <li>").append(line).append("</li>\n");
            }
            builder.append("    </ol>\n").append("  <li>\n");
        }
        builder.append("</ol>");
        return builder.toString();
    }
}
