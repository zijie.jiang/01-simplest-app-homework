package com.twuc.webApp;

import com.twuc.webApp.model.Answer;
import com.twuc.webApp.model.Question;
import com.twuc.webApp.services.TableService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;


import java.util.List;

import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_JSON;

@RestController
@RequestMapping("/api")
public class TableController {

    private final TableService tableService;

    public TableController(TableService tableService) {
        this.tableService = tableService;
    }

    @GetMapping("/tables/plus")
    public List<List<String>> plusTable(@RequestParam(defaultValue = "1") Integer start,
                                        @RequestParam(defaultValue = "9") Integer end) {
        if (start > 0 && start < end) {
            return tableService.plusTable(start, end);
        }
        throw new IllegalArgumentException("please input valid range");
    }

    @GetMapping("/tables/multiply")
    public String multiplyTable(@RequestParam(defaultValue = "1") Integer start,
                                @RequestParam(defaultValue = "9") Integer end) {
        if (start > 0 && end > 0 && start < end) {
            return tableService.multiplyTable(start, end);
        }
        throw new IllegalArgumentException("please input valid range");
    }

    @PostMapping("/check")
    public ResponseEntity<Answer> check(@RequestBody @Valid Question question) {
        return ResponseEntity.status(OK)
                .contentType(APPLICATION_JSON)
                .body(tableService.check(question));
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<Void> handleException(Exception e) {
        return ResponseEntity.badRequest().build();
    }
}
